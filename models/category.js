/** 
*  Category model
*  Describes the characteristics of each attribute in an order resource.
*
* @author Lavanya Reddy Uppula <s534574@nwmissouri.edu>
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')
const CategorySchema = new mongoose.Schema({
    category_id: { type: Number, required: true},
    category_name: { type: String, required: true, default: 'category_name' },
    category_amount: { type: Number, required: true },
  
})
module.exports = mongoose.model('Category', CategorySchema)