/** 
*  Account model
*  Describes the characteristics of each attribute in an Account - one entry on a customer's order.
*
* @author Sai Nikhil Pippara<S534737@nwmissouri.edu>
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const AccountSchema = new mongoose.Schema({
  _id: { type: Number, required: true},
  AccountNumber: { type: Number, required: true },
  AccountType: { type: String, required: true , default:'Checking account' },
  debitCard: { type: Number, required: true },
  creditCard: { type: Number, required: false },
  loans:{type:String,required:false,default:'NA'},
  
})

module.exports = mongoose.model('Account', AccountSchema)