/** 
*  transaction model
*  Describes the characteristics of each attribute in an transaction resource.
*
* @author Rudra Teja Potturi <S534804@nwmissouri.edu>
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const TransactionSchema = new mongoose.Schema({
  
  transactionID: { type: Number, required: true },  
  amount: { type: Number, required: true, default: 0},  
  transactionDate: { type: Date, required: true,default:Date.now()},
  postedDate:{type:Date,required:true,default:Date.now()},
  transactionType: { type: String, required: true, default: 'NA'},
  cardNumber: {type: String, required: true, default: '0000 0000 0000 0000'},
  transactionStatus:{type:Boolean, required: true,default:false},
  
})

module.exports = mongoose.model('Transaction', TransactionSchema)
