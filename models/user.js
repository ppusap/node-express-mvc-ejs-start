/** 
*  User model
*  Describes the characteristics of each attribute in a User resource.
*
* @author Pratyusha Pusapati <S534642@nwmissouri.edu>
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const UserSchema = new mongoose.Schema({
  _id: { type: Number, required: true},
  SSN: { type: Number, required: false },
  firstName: { type: String, required: true, default: 'firstName' },
  lastName: { type: String, required: true, default: 'LastName' },
  phoneNumber:{type:Number,required:true },
  email: { type: String, required: true },
  
  annualIncome: {type:Number,required:true},
  address: { type: String, required: true, default: '' },
  
  zip: { type: Number, required: true, default: 64468 },
  country: { type: String, required: true, default: 'USA' }
})

module.exports = mongoose.model('User', UserSchema)
