// set up a temporary (in memory) database
/**
 * @author Pratyusha Pusapati <S534642@nwmissouri.edu>
 */
const Datastore = require('nedb')
const LOG = require('../utils/logger.js')

// require each data file

const users = require('../data/users.json')
const categories = require('../data/categories.json')
const transactions = require('../data/transactions.json')
const accounts = require('../data/accounts.json')


// inject the app to seed the data

module.exports = (app) => {
  LOG.info('START seeder.')
  const db = {}

 


  // user don't depend on anything else...................

  db.users = new Datastore()
  db.users.loadDatabase()

  // insert the sample data into our data store
  db.users.insert(users)

  // initialize app.locals (these objects will be available to our controllers)
  app.locals.users = db.users.find(users)
  LOG.debug(`${app.locals.users.query.length} user seeded`)

  // category don't depend on anything else .....................

  db.categories = new Datastore()
  db.categories.loadDatabase()

  // insert the sample data into our data store
  db.categories.insert(categories)

  // initialize app.locals (these objects will be available to our controllers)
  app.locals.categories = db.categories.find(categories)
  LOG.debug(`${app.locals.categories.query.length} category seeded`)

  // transaction need a user beforehand .................................

  db.transactions = new Datastore()
  db.transactions.loadDatabase()

  // insert the sample data into our data store
  db.transactions.insert(transactions)

  // initialize app.locals (these objects will be available to our controllers)
  app.locals.transactions = db.transactions.find(transactions)
  LOG.debug(`${app.locals.transactions.query.length} transaction seeded`)

  // Each account needs a category and an transaction beforehand ...................

  db.accounts = new Datastore()
  db.accounts.loadDatabase()

  // insert the sample data into our data store
  db.accounts.insert(accounts)

  // initialize app.locals (these objects will be available to our controllers)
  app.locals.accounts = db.accounts.find(accounts)
  LOG.debug(`${app.locals.accounts.query.length} account seeded`)

  


  LOG.info('END Seeder. Sample data read and verified.')
}
