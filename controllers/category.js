/** 
*  category controller
*  Handles requests related to categories (see routes)
*
* @author Lavanya Reddy Uppula <s534574@nwmissouri.edu>
*
*/
const express = require('express')
const api = express.Router()
const LOG = require('../utils/logger.js')
const find = require('lodash.find')
const remove = require('lodash.remove')
const Model = require('../models/category.js')
const notfoundstring = 'category not found'

// RESPOND WITH JSON DATA  --------------------------------------------

// GET all JSON
api.get('/findall', (req, res) => {
  res.setHeader('Content-Type', 'application/json')
  const data = req.app.locals.categories.query
  res.send(JSON.stringify(data))
})

// GET one JSON by ID
api.get('/findone/:id', (req, res) => {
  res.setHeader('Content-Type', 'application/json')
  const id = parseInt(req.params.id)
  const data = req.app.locals.categories.query
  const item = find(data, { category_id: id })
  if (!item) { return res.end(notfoundstring) }
  res.send(JSON.stringify(item))
})

// RESPOND WITH VIEWS  --------------------------------------------

// GET to this controller base URI (the default)
api.get('/', (req, res) => {
  res.render('category/index.ejs')
})

// GET create
api.get('/create', (req, res) => {
  LOG.info(`Handling GET /create${req}`)
  const item = new Model()
  LOG.debug(JSON.stringify(item))
  res.render('category/create',
    {
      title: 'Create category',
      layout: 'layout.ejs',
      category: item
    })

})

// GET /delete/:id
api.get('/delete/:id', (req, res) => {
  LOG.info(`Handling GET /delete/:id ${req}`)
  const id = parseInt(req.params.id, 10)
  const data = req.app.locals.categories.query
  const item = find(data, { category_id: id })
  if (!item) { return res.end(notfoundstring) }
  LOG.info(`RETURNING VIEW FOR ${JSON.stringify(item)}`)
  return res.render('category/delete.ejs',
    {
      title: 'Delete category',
      layout: 'layout.ejs',
      category: item
    })

})

// GET /details/:id
api.get('/details/:id', (req, res) => {
  LOG.info(`Handling GET /details/:id ${req}`)
  const id = parseInt(req.params.id, 10)
  const data = req.app.locals.categories.query
  const item = find(data, { category_id: id })
  if (!item) { return res.end(notfoundstring) }
  LOG.info(`RETURNING VIEW FOR ${JSON.stringify(item)}`)
  return res.render('category/details.ejs',
    {
      title: 'category Details',
      layout: 'layout.ejs',
      category: item
    })

})

// GET one
api.get('/edit/:id', (req, res) => {
  LOG.info(`Handling GET /edit/:id ${req}`)
  const id = parseInt(req.params.id, 10)
  const data = req.app.locals.categories.query
  const item = find(data, { category_id: id })
  if (!item) { return res.end(notfoundstring) }
  LOG.info(`RETURNING VIEW FOR${JSON.stringify(item)}`)
  return res.render('category/edit.ejs',
    {
      title: 'categories',
      layout: 'layout.ejs',
      category: item
    })

})

// HANDLE EXECUTE DATA MODIFICATION REQUESTS --------------------------------------------

// POST new
api.post('/save', (req, res) => {
  LOG.info(`Handling POST ${req}`)
  LOG.debug(JSON.stringify(req.body))
  const data = req.app.locals.categories.query
  const item = new Model()
  LOG.info(`NEW ID ${req.body.category_id}`)
  item.category_id = parseInt(req.body.category_id)
  item.category_name = req.body.category_name
  item.category_amount = parseInt(req.body.category_amount)
  data.push(item)
  LOG.info(`SAVING NEW category ${JSON.stringify(item)}`)
 return res.redirect('/category')

})

// POST update
api.post('/save/:id', (req, res) => {
  LOG.info(`Handling SAVE request ${req}`)
  const id = parseInt(req.params.id)
  LOG.info(`Handling SAVING ID=${id}`)
  const data = req.app.locals.categories.query
  const item = find(data, { category_id: id })
  if (!item) { return res.end(notfoundstring) }
  LOG.info(`ORIGINAL VALUES ${JSON.stringify(item)}`)
  LOG.info(`UPDATED VALUES: ${JSON.stringify(req.body)}`)
  item.category_name = req.body.category_name
  item.category_amount =parseInt(req.body.category_amount) 
  LOG.info(`SAVING UPDATED category ${JSON.stringify(item)}`)
 return res.redirect('/category')

})

// DELETE id (uses HTML5 form method POST)
api.post('/delete/:id', (req, res) => {
  LOG.info(`Handling DELETE request ${req}`)
  const id = parseInt(req.params.id)
  LOG.info(`Handling REMOVING ID=${id}`)
  const data = req.app.locals.categories.query
  const item = find(data, { category_id: id })
  if (!item) { return res.end(notfoundstring) }
  if (item.isActive) {
    item.isActive = false
    console.log(`Deacctivated item ${JSON.stringify(item)}`)
  } else {
    const item = remove(data, { category_id: id })
    console.log(`Permanently deleted item ${JSON.stringify(item)}`)
  }
 return res.redirect('/category')

})

module.exports = api
